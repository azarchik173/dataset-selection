import React, { useEffect, useState, useCallback } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Mark from "../Mark";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 10px 0;
  border-top: 1px solid rgba(187, 183, 183, 0.27);
`;
const AdminPhotos = styled.span`
  &:last-child{
    margin-bottom:62px;
  }
`;
const PhotoImage = styled.div`
    background-image: url(${(props) => props.src});
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
    border-radius: 5px;
    width: ${(props) => (props.width ? props.width : 0)}px;
    height: ${(props) => (props.height ? props.height : 0)}px;
    ${props => props.maxHeight && `max-height: ${props.maxHeight}px;`}
    @media (max-width: 1920px), (min-width: 1920px) {
        max-width: 1300px;
    }
    @media (max-width: 1600px) {
        max-width: 82%;
    }
    @media (max-width: 1024px) {
        max-width: 832px;
    }
    @media (max-width: 832px) {
        max-width: 99%;
    }
`;
const MarksContainer = styled.div`
  margin-top: 10px;
  display: flex;
  width: 300px;
  justify-content: ${(props) => (props.selected ? "center" : "space-between")};
`;

const marks = [
  {
    mark: 1,
    color: "rgb(255, 0, 0)",
  },
  {
    mark: 1.5,
    color: "rgb(255, 64, 0)",
  },
  {
    mark: 2,
    color: "rgb(255, 128, 0)",
  },
  {
    mark: 2.5,
    color: "rgb(255, 180, 0)",
  },
  {
    mark: 3,
    color: "rgb(255, 255, 0)",
  },
  {
    mark: 3.5,
    color: "rgb(180, 255, 0)",
  },
  {
    mark: 4,
    color: "rgb(128, 255, 0)",
  },
  {
    mark: 4.5,
    color: "rgb(64, 255, 0)",
  },
  {
    mark: 5,
    color: "rgb(0, 255, 0)",
  },
];

export const Photo = ({ refer, src, markPhoto, screenSize, marksArray }) => {
  const [markSelect, updateSelect] = useState({
    select: false,
    mark: null,
  });
  
  const [size, sizeUpdate] = useState({
    height: null,
    width: null,
    maxHeight: null
  });

  const getMaxHeight = useCallback((width, height) => {
    if(width && height) {
      let maxHeightImage = null;
      const proportion = width / height;
      if(screenSize.width >= 1600) maxHeightImage = +(1300/proportion).toFixed(1)
      else if(screenSize.width >= 1024) maxHeightImage = +((screenSize.width-20)*0.8125 / proportion).toFixed(1)
      else if(screenSize.width >= 832) maxHeightImage = +(832/proportion).toFixed(1)
      else maxHeightImage = +((screenSize.width-20)*0.99 / proportion).toFixed(1)
      const generalHeight = screenSize.height-100;
      return (maxHeightImage > generalHeight ? generalHeight : maxHeightImage )
    }
  }, [screenSize])

  useEffect(() => {
    sizeUpdate((prevImageSize) => {
      if(prevImageSize.width && prevImageSize.height) {
        return { ...prevImageSize, maxHeight: getMaxHeight(prevImageSize.width, prevImageSize.height)}
      }
      return prevImageSize
    })
  }, [screenSize, getMaxHeight])
    

  useEffect(() => {
    var photo = new Image();
    photo.onload = function () {
      sizeUpdate({
        height: this.height,
        width: this.width,
        maxHeight: getMaxHeight(this.width, this.height)
      });
    };
    photo.src = src;
  }, [src, getMaxHeight]);
  return (
    <Container {...refer && {ref: refer}} >
      <PhotoImage
        src={src}
        height={size.height}
        width={size.width}
        maxHeight={size.maxHeight}
      />
      {markPhoto ?
        <MarksContainer selected={markSelect.select}>
          {!markSelect.select &&
            marks.map((eachMark, index) => (
              <Mark
                key={eachMark.mark}
                color={eachMark.color}
                mark={eachMark.mark}
                markSelected={() => {
                  if (!markSelect.select) {
                    markPhoto(eachMark.mark);
                  }
                  updateSelect({ select: true, mark: eachMark.mark });
                }}
              />
            ))}
          {markSelect.select && (
            <div style={{height: '30px'}}>Вы оценили фотографию на: {markSelect.mark}</div>
          )}
        </MarksContainer>
      : marksArray.map((mark, index) => (
        <AdminPhotos key={index}>{mark.userName}: {mark.rating}&nbsp;</AdminPhotos>
      ))}
    </Container>
  );
};

Photo.propTypes = {
  src: PropTypes.string.isRequired,
  markPhoto: PropTypes.func,
  refer: PropTypes.object,
  screenSize: PropTypes.object,
  marksArray: PropTypes.array
};
