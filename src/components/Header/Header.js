import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { Button } from "antd";
import logoImg from "../../assets/images/logo.png";
import { UserInformation } from "../UserInformation/UserInformation";

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
  max-height: 600px;
  border-radius: 10px;
  box-shadow: 4px 2px 11px 0px rgba(0, 0, 0, 0.27);
  margin-top: 5px;
`;
const Logo = styled.div`
  background-image: url(${logoImg});
  background-repeat: no-repeat;
  background-size: contain;
  background-position: center;
  @media (min-width: 1920px), (max-width: 1920px) {
    height: 250px;
    width: 250px;
  }
  @media (max-width: 1600px) {
    height: 230px;
    width: 230px;
  }
  @media (max-width: 1366px) {
    height: 210px;
    width: 210px;
  }
  @media (max-width: 1024px) {
    height: 190px;
    width: 190px;
  }
  @media (max-width: 832px) {
    height: 170px;
    width: 170px;
  }
`;
const WelcomeText = styled.div`
  font-family: Roboto;
  padding: 4px 30px 10px;
  text-align: center;
`;
const ButtonStyled = styled(Button)`
  margin: 10px 0;
`;

export const Header = ({
  isAuth,
  scrollClick,
  userData,
  auth,
  logout,
  buttonLoading,
  counts = [],
  updateClick
}) => {
  return (
    <HeaderContainer>
      <UserInformation
        admin={userData.userName === 'admin'}
        isAuth={isAuth}
        {...userData}
        auth={auth}
        logout={logout}
        buttonLoading={buttonLoading}
        adminCounts={counts}
        updateClick={updateClick}
      />
      <Logo />
      <WelcomeText>
        Приветствуем Вас на проекте <b>InsighTech</b>. В ближайшее время, мы
        хотим запустить интересное <b>мобильное приложение</b>, которое поможет
        многим пользователем интернета в <b>оценке их одежды</b>. Согласитесь,
        часто люди сталкиваются с выбором одежды из гардероба. Сегодня, у{" "}
        <b>вас</b> есть возможность стать <b>частью</b> этого крупного проекта,
        просто оценив несколько фотографий! Оценивая каждую из фотографий ниже,
        Вы помогаете нам сделать
        <b> качество</b> приложение только лучше! При оценивании, учитывайте
        пожалуйста <b>только насколько красива одежда</b> человека, не учитвая
        его внешние качества
      </WelcomeText>
      {isAuth && (
        <ButtonStyled onClick={scrollClick}>Перейти к фотографиям</ButtonStyled>
      )}
    </HeaderContainer>
  );
};

Header.propTypes = {
  isAuth: PropTypes.bool.isRequired,
  scrollClick: PropTypes.func.isRequired,
  userData: PropTypes.object.isRequired,
  auth: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  buttonLoading: PropTypes.bool.isRequired,
  counts: PropTypes.array,
  updateClick: PropTypes.func
};
