import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const MarkStyled = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: ${props => props.color};
    border-radius: 50px;
    cursor: pointer;
    min-width: 30px;
    min-height: 30px;
    font-family: Roboto;
    &:hover {
        background-color: ${props => props.color};
        opacity: 0.5;
    }
`

export const Mark = ({ mark, color, markSelected }) => {
    return (
       <MarkStyled color={color} onClick={markSelected}>{mark}</MarkStyled>
    )
}

Mark.propTypes = {
    mark: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    markSelected: PropTypes.func.isRequired
}