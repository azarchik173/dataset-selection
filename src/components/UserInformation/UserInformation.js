import React from "react";
import styled from "styled-components";
import { Button, Form, Input } from "antd";
import PropTypes from "prop-types";

const ContainerStyled = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  padding: 10px 10px 0 10px;
`;
const StyledText = styled.span`
  font-family: Roboto;
  max-width: 50vw;
  white-space: break-spaces;
  margin-right: 10px;
`;
const FormStyled = styled(Form)`
  display: inline-flex;
  max-height: 32px;
`;
export const UserInformation = ({
  admin = false,
  adminCounts = [],
  isAuth,
  userName,
  photosCount,
  auth,
  logout,
  buttonLoading,
  updateClick
}) => {
  const [form] = Form.useForm();

  const submitClick = (data) => {
    if (data) {
      auth(data);
    } else logout();
  };

  return (
    <ContainerStyled>
      {!isAuth ? (
        <StyledText>
          Вы <b>не авторизованы</b>
        </StyledText>
      ) : (
        admin
        ? <StyledText>
          Оценившие юзеры: <br />
          {adminCounts.map((data, index) => (
            <span key={index}>{data.login}: <b>{data.count}</b> фото <br /></span>
          ))}
          <button onClick={() => updateClick()}>Обновить</button>
        </StyledText>
        :<StyledText>
          Ваш логин:
          <b>
            <i> {userName}</i>
          </b>
          <br />
          За всё время Вы оценили
          <b> {photosCount} </b>
          фотографий
        </StyledText>
      )}
      {!isAuth ? (
        <FormStyled form={form} layout="inline" onFinish={submitClick}>
          <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: "Поле логин должно быть заполнено",
              },
            ]}
          >
            <Input placeholder="Логин" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "Поле пароль должно быть заполнено",
              },
            ]}
          >
            <Input type="password" placeholder="Пароль" />
          </Form.Item>
          <Form.Item shouldUpdate>
            {() => (
              <Button
                loading={buttonLoading}
                type="primary"
                htmlType="submit"
                disabled={
                  !form.isFieldsTouched(true) ||
                  form.getFieldsError().filter(({ errors }) => errors.length)
                    .length
                }
              >
                Войти
              </Button>
            )}
          </Form.Item>
        </FormStyled>
      ) : (
        <Button type="primary" onClick={() => submitClick(null)}>
          Выйти
        </Button>
      )}
    </ContainerStyled>
  );
};

UserInformation.propTypes = {
  admin: PropTypes.bool,
  adminCounts: PropTypes.array,
  isAuth: PropTypes.bool.isRequired,
  userName: PropTypes.string,
  photosCount: PropTypes.number,
  auth: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  buttonLoading: PropTypes.bool.isRequired,
  updateClick: PropTypes.func
};
