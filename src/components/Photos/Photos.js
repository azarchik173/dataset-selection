import React, { useState, useEffect, useCallback } from "react";
import { useCookies } from "react-cookie";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Button, Spin, message, Pagination } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

import Photo from "../Photo";
import { getPhotos, postMarkPhoto, getPhotosCount } from "../../axios";

const Container = styled.div`
  margin: 20px 0;
  border-radius: 5px;
  padding: 10px;
`;
const SpinStyled = styled(Spin)`
  display: flex;
  justify-content: center;
`;
const PaginationContainer = styled.div`
  position:fixed;
  bottom: 10px;
  left: 0;
  display: flex;
  justify-content: center;
  width: 100%;
  min-width: 500px;
  background-color: grey;
  padding: 10px 0;
`

const url = "http://35.234.110.58:8000";
const photoUrl = "http://35.234.110.58";

let resizeTimer = null;
let scrollLoadOn = false;

export const Photos = ({ isAdmin, scrollToPhoto, onMark }) => {
  const [{ authData }] = useCookies(["authData"]);
  const [lazyOn, updateLazy] = useState(false);
  const [photosList, updatePhotos] = useState([]);
  const [adminPhotosList, updateAdminList] = useState([]);
  const [visibleCounter, setVisibleCounter] = useState(0);
  const [pagination, setPagination] = useState(1);
  const [screenSize, screenSizeUpdate] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });
  const bottomUpdate = useCallback(() => {
    updateLazy(true);
    getPhotos(`${url}/api/photo`, isAdmin ? visibleCounter : 0, 10, authData)
      .then((response) => {
        let counter = visibleCounter
        if(!isAdmin) {
          response.data.forEach((data) => {
            if (
              photosList.filter(
                (photo) =>
                  photo.id === data.id && photo.src === `${photoUrl}/${data.path}`
              ).length === 0
            ) {
              updatePhotos((prevPhotos) => [
                ...prevPhotos,
                { id: data.id, src: `${photoUrl}/${data.path}` },
              ]);
              counter ++;
            }
          });
        } else {
          response.data.photos.forEach(data => {
            const markArray = []
            data.assessments.forEach(mark => {
              const userName = response.data.users.filter(user => user.id === mark.user_id)[0].login
              markArray.push({
                rating: mark.rating,
                userName
              })
            })
            updateAdminList((prevState) => [
              ...prevState,
              {
                id: data.id,
                src: `${photoUrl}/${data.path}`,
                marks: markArray
              }
            ])
            counter++
          })
        }
        setVisibleCounter(counter)
        scrollLoadOn = false;
      })
      .catch((e) => message.error("Произошла ошибка во время загрузки"))
      .finally(() => updateLazy(false));

  }, [authData, isAdmin, photosList, visibleCounter]);
  const lazyLoading = (countParam) => {
    let offset = 0;
    if(isAdmin) {
      offset = countParam * 10
    }
    getPhotos(`${url}/api/photo`, offset, 10, authData)
      .then((response) => {
        let counter = visibleCounter
        if(!isAdmin) {
          response.data.forEach((data) => {
            if (
              photosList.filter(
                (photo) =>
                  photo.id === data.id && photo.src === `${photoUrl}/${data.path}`
              ).length === 0
            ) {
              updatePhotos((prevPhotos) => [
                ...prevPhotos,
                { id: data.id, src: `${photoUrl}/${data.path}` },
              ]);
              counter++;
            }
          });
        } else {
          response.data.photos.forEach(data => {
            const markArray = []
            data.assessments.forEach(mark => {
              const userName = response.data.users.filter(user => user.id === mark.user_id)[0].login
              markArray.push({
                rating: mark.rating,
                userName
              })
            })
            updateAdminList((prevState) => [
              ...prevState,
              {
                id: data.id,
                src: `${photoUrl}/${data.path}`,
                marks: markArray
              }
            ])
            counter++
          })
        }
        setVisibleCounter(counter);
      })
      .catch((e) => message.error("Произошла ошибка во время загрузки"))
      .finally(() => updateLazy(false));
  };

  const updateScreenSize = (e) => {
    screenSizeUpdate((prevState) => {
      if (
        Math.abs(prevState.width - e.target.innerWidth) >= 70 &&
        Math.abs(prevState.height - e.target.innerHeight) >= 70
      ) {
        return {
          width: e.target.innerWidth,
          height: e.target.innerHeight,
        };
      }
      return prevState;
    });
  };

  const scrollOn = useCallback(() => {
    const wrapElement = document.getElementById("root");
    if (
      Math.floor(wrapElement.getBoundingClientRect().bottom) <=
        window.innerHeight + 400 &&
      !scrollLoadOn
    ) {
      if(!isAdmin) {
        scrollLoadOn = true;
        bottomUpdate();
      }
    }
  }, [bottomUpdate, isAdmin]);

  const resizeEvent = useCallback((e) => {
    resizeTimer && clearTimeout(resizeTimer);
    resizeTimer = setTimeout(() => updateScreenSize(e), 700);
  }, []);

  useEffect(() => {
    getPhotosCount(url, authData).then((responseCount) => {
      getPhotos(`${url}/api/photo`, 0, 10, authData)
        .then((response) => {
          let counter = visibleCounter
          if(!isAdmin) {
            response.data.forEach((data) => {
              updatePhotos((prevPhotos) => [
                ...prevPhotos,
                { id: data.id, src: `${photoUrl}/${data.path}` },
              ]);
              counter++;
            });
          } else {
            response.data.photos.forEach(data => {
              const markArray = []
              data.assessments.forEach(mark => {
                const userName = response.data.users.filter(user => user.id === mark.user_id)[0].login
                markArray.push({
                  rating: mark.rating,
                  userName
                })
              })
              updateAdminList((prevState) => [
                ...prevState,
                {
                  id: data.id,
                  src: `${photoUrl}/${data.path}`,
                  marks: markArray
                }
              ])
              counter++;
            })
          }
          setVisibleCounter(counter)
        })
        .catch((e) => message.error("Произошла ошибка во время загрузки"));
    }, []);

    document.addEventListener("resize", resizeEvent);

    return () => {
      updatePhotos([]);
      document.removeEventListener("resize", resizeEvent);
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [resizeEvent, authData, isAdmin]);

  useEffect(() => {
    document.addEventListener("scroll", scrollOn);
    return () => document.removeEventListener("scroll", scrollOn);
  }, [scrollOn]);

  return (
    <Container>
      {isAdmin &&
      <PaginationContainer>
        <Pagination 
          current={pagination}
          onChange={(target) => {
            updateAdminList([])
            setPagination(target)
            updateLazy(true)
            lazyLoading(target)
          }}
          total={19160}
          pageSizeOptions={[]}
          disabled={lazyOn}
        ></Pagination>
      </PaginationContainer>
      }
      {!isAdmin ? photosList.map((photo, ind) => (
        <Photo
          refer={ind === 0 ? scrollToPhoto : null}
          key={photo.id}
          src={photo.src}
          markPhoto={(mark) => {
            postMarkPhoto(`${url}/api/photo`, photo.id, mark, authData)
              .then(() => onMark())
              .catch((e) =>
                message.error("Произошла ошибка во время загрузки")
              );
          }}
          screenSize={screenSize}
        />
      )) : adminPhotosList.map((photo, ind) => (
        <Photo
          refer={ind === 0 ? scrollToPhoto : null}
          key={photo.id}
          src={photo.src}
          marksArray={photo.marks}
          screenSize={screenSize}
        />
      ))}
      {photosList.length > 0 && !isAdmin && (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button type="primary" onClick={lazyLoading} loading={lazyOn}>
            Загрузить ещё
          </Button>
        </div>
      )}
      {((photosList.length === 0 && !isAdmin) || (isAdmin && lazyOn)) && (
        <SpinStyled
          indicator={<LoadingOutlined style={{ fontSize: 45 }} spin={true} />}
        />
      )}
    </Container>
  );
};

Photos.propType = {
  photos: PropTypes.arrayOf(PropTypes.object).isRequired,
  scrollToPhoto: PropTypes.object.isRequired,
  onMark: PropTypes.func.isRequired,
};
