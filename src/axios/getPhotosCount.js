import axios from "axios";

export const getPhotosCount = (url, cookie) =>
  axios.get(`${url}/api/photo/count`, {
    headers: {
      Authorization: `Bearer ${cookie}`,
    },
  });
