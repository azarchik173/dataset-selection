import axios from "axios";

export const postMarkPhoto = (url, photoId, rating, cookie) =>
  axios.post(
    url,
    {
      photo_id: photoId,
      rating,
    },
    {
      headers: {
        Authorization: `Bearer ${cookie}`,
      },
    }
  );
