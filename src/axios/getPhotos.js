import axios from "axios";

export const getPhotos = (url, offset, limit, cookie) =>
  axios.get(url, {
    params: {
      offset,
      limit,
    },
    headers: {
      Authorization: `Bearer ${cookie}`,
    },
  });
