import axios from "axios";

export const getUserCount = (url, cookie) =>
  axios.get(url, {
    headers: {
      Authorization: `Bearer ${cookie}`,
    },
  });
