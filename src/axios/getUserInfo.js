import axios from "axios";

export const getUserInfo = (url, cookie) =>
  axios.get(url, {
    headers: {
      Authorization: `Bearer ${cookie}`,
    },
  });
