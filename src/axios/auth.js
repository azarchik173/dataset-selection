import axios from "axios";

export const auth = (url, { username, password }) =>
  axios.post(url, {
    login: username,
    password,
  });
