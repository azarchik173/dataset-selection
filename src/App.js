import React, { useRef, useState, useEffect } from "react";
import { useCookies } from "react-cookie";
import { message } from "antd";
import axios from "axios";

import { auth as axiosAuth, getUserInfo, getUserCount } from "./axios";
import Header from "./components/Header";
import Photos from "./components/Photos";
import "./App.css";

const url = "http://35.234.110.58:8000";

function App() {
  const scrollToFisrt = useRef(null);
  const [cookie, setCookie, removeCookie] = useCookies(["authData"]);
  const [loginButtonLoading, setLoadingButton] = useState(false);
  const [isAutorized, setAutorized] = useState(false);
  const [allInfo, setInfoData] = useState([])
  const [userData, setUserData] = useState({
    userName: "",
    photosCount: 0,
  });
  const auth = (loginData) => {
    setLoadingButton(true);
    axiosAuth(`${url}/api/auth`, loginData)
      .then(({ data }) => {
        setCookie("authData", data.access_token);
      })
      .catch(() => message.error("Во время авторизации произошла ошибка!"))
      .finally(() => setLoadingButton(false));
  };
  const logout = () => {
    removeCookie("authData");
    setAutorized(false);
  };

  useEffect(() => {
    if (cookie.authData) {
      axios
        .all([
          getUserInfo(`${url}/api/users/me`, cookie.authData),
          getUserCount(`${url}/api/users/count_photos`, cookie.authData),
        ])
        .then(
          axios.spread(({ data }, countResponse) => {
            message.success(`Добро пожаловать, ${data.login}`);
            setUserData({
              userName: data.login,
              photosCount: data.login === 'admin' ? 0 : countResponse.data.count,
            });
            if(data.login === 'admin') {
              const counts = countResponse.data
              setInfoData(counts)
            }
            setAutorized(true);
          })
        );
    }
  }, [cookie]);

  return (
    <div className="App">
      <Header
        isAuth={isAutorized}
        buttonLoading={loginButtonLoading}
        logout={logout}
        auth={auth}
        userData={userData}
        counts={allInfo}
        scrollClick={() => {
          return scrollToFisrt && scrollToFisrt.current
            ? scrollToFisrt.current.scrollIntoView({ behavior: "smooth" })
            : message.warning("Дождитесь окончания загрузки фотографий");
        }}
        updateClick={() => {
          getUserCount(`${url}/api/users/count_photos`, cookie.authData)
          .then((countResponse) => {
              if(userData.userName === 'admin') {
                const counts = countResponse.data
                setInfoData(counts)
              }
          })
        }}
      />
      {isAutorized && (
        <Photos
          isAdmin={userData.userName === 'admin'}
          onMark={() =>
            setUserData((prevState) => ({
              ...prevState,
              photosCount: prevState.photosCount + 1,
            }))
          }
          scrollToPhoto={scrollToFisrt}
        />
      )}
    </div>
  );
}

export default App;
